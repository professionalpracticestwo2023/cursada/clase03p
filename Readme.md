### El siguiente repositorio tiene como intención que el alumno pueda realizar envíos de correos desde Python utilizando gmail de ejemplo.

### Se pide leer el link para una mayor información con respecto a la seguridad y token de gmail

    - https://kinsta.com/es/blog/gmail-smtp-servidor/

### Actividades sugeridas

    - Clonar el repositorio y generar una rama con las actividades sugeridas
    - Crear un archivo en donde se almacene el usuario de correo y el token , para que no quede en el codigo
    - Crear un archivo con el contenido del mail, para que el mismo que este en el codigo
    - Generar un archivo de datos que contenga la siguiente estructura

            - Apellido | Nombre | Email

    - Realizar las modificaciones necesarias para que el script lea el archivo de candidatos a enviar el mail y realice el envio del mismo.